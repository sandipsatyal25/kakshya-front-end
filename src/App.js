import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Login from './Components/Login'
import Dashboard from './Components/Dashboard'
import Navbar from './Components/Navbar'


function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Navbar />
    <Switch>
    <Route path="/login" component={Login} />
    <Route path="/dashboard" component={Dashboard}/>
    </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App;
