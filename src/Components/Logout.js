import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
class Logout extends Component {
    
    handleClick = ()=>{
        localStorage.removeItem('token');
        this.props.pushLoginCred(false);
        this.props.history.push('/login');
    }

    render() {
        if(!localStorage.getItem('token')){
            return (
                <div>
                <p>You havent logged in yet.</p>
                <Link to="/login">Login now</Link>
                </div>
            )
        } else{
        return (
            <div>
                <button onClick={this.handleClick} className="custom-no-style-button">Logout</button>
            </div>
        )
    }
}
}

const mapDispatchToProps = (dispatch)=>{
    return{
        pushLoginCred : (bool)=>{
            dispatch({type:'LOGIN_CHECK', data:bool})
        }
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Logout))