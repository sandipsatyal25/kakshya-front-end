import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import LoggedInNav from './Navbar/LoggedInNav'
import LoggedOutNav from './Navbar/LoggedOutNav'

class Navbar extends Component {

    render() {
        const template = this.props.loginCred ? (<LoggedInNav />) : (<LoggedOutNav />)
        return (
            <nav className="red">
            <div className="container nav-wrapper">    
            {template}
            </div>
        </nav>
        )
    }
}

const mapStateToProps = (state)=>{
    return{
        loginCred : state.UserLogin
    }
}

export default connect(mapStateToProps)(withRouter(Navbar))