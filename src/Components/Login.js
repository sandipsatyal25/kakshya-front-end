import React, { Component } from 'react'
import axios from 'axios';
import {connect} from 'react-redux'

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : '',
            password : '',
            token : ''
        }
    }


    handleChange = (e)=>{
        const {id, value} = e.target;
        this.setState({
            [id] : value
        })
    }

    handleClick = (e)=>{
        e.preventDefault();
        this.getToken(this.state.email, this.state.password);
    }

    getToken = async(email, password) =>{
        await axios.post('https://reqres.in/api/login',{email:email, password:password})
        .then(res=>{
            console.log(res.data.token)
            let token = res.data.token;
            this.props.pushLoginCred(true);
            this.setState({
                token :token
            })
            return this.state.token
        })
        .then((token)=>{
            this.setToken(token);
        })
        .catch(err=>console.log(err))
    }

    setToken = (token)=>{
        localStorage.setItem('token', token);
        this.props.history.push('/dashboard');
        // console.log(token);
    }

    render() {
        if(localStorage.getItem('token')){
            this.props.pushLoginCred(true);
            this.props.history.push('/dashboard');
            return null
        }
        else{
        return (
            <div className="container">
            <p>eve.holt@reqres.in  and  cityslicka"</p>
            <div className="row">
            <div className="post card z-depth-3">
            <div className="card-content">
            <h4>Login Form</h4>
            <form>
            <div className="col s12 m6">
            <input type="email" id="email" placeholder="email" onChange={this.handleChange} />
            </div>
            <div className="col s12 m6">
            <input type="password" id="password" placeholder="password" onChange={this.handleChange}/>
            </div>
            <div className="row">
            <div className="col s12">
            <input type="submit" value="submit" onClick={this.handleClick} className="btn right z-depth-5"/>
            </div>
            </div>
            </form>
            </div>
            </div>
            </div>
            </div>
        )
    }
}
}

const mapDispatchToProps = (dispatch)=>{
    return { 
        pushLoginCred : (bool)=>{
            dispatch({type:'LOGIN_CHECK', data:bool})
        }
    }
}

export default connect(null, mapDispatchToProps)(Login)