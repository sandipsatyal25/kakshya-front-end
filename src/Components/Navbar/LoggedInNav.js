import React from 'react'
import Logout from '../Logout';

const LoggedInNav = ()=>{
    return(
        <div>
        <a href="/" className="brand-logo">Logo</a>
        <ul className="right">
        <li><a href="/dummy">UserName</a></li>
        <li><Logout/></li>
        </ul>
        </div>
    )
}

export default LoggedInNav