import React from 'react'

const LoggedOutNav = ()=>{
    return(
        <div>
        <a href="/" className="brand-name">Logo</a>
        <ul className="right">
        <li><a href="/login">Login</a></li>
        <li><a href="/signup">Signup</a></li>
        </ul>
        </div>
    )
}

export default LoggedOutNav