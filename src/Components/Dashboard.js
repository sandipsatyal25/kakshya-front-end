import React, { Component } from 'react'
import {connect} from 'react-redux'
import StatusBar from './Dashboard/StatusBar'
import MenuBar from './Dashboard/MenuBar'

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            callerCred : {
                type : 'student',
                name : 'lorem ipsum'
            }
        }
    }
    render() {
        if(!localStorage.getItem('token')){
            this.props.pushLoginCred(false)
            return(
                <div>
                <p>You havent logged in yet</p>
                <a href="/login">Login now</a>
                </div>
            )
        } else{
            this.props.pushLoginCred(true);
        return (
            <div>
                <StatusBar name={this.state.callerCred.name}/>
                <MenuBar />
            </div>
        )
    }
}
}

const mapDispatchToProps = (dispatch)=>{
    return{
        pushLoginCred : (bool)=>{
            dispatch({type:'LOGIN_CHECK', data:bool})
        }
    }
}
export default connect(null, mapDispatchToProps)(Dashboard)