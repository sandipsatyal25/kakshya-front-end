import React from 'react'


class StatusBar extends React.Component{
    render(){
        return(
            <div className="container">
                <div className="post card">
                    <div className="card-content">
                        <a href="/faq#reward" className="no-link-style">
                        <p title="Read more">Welcome back {this.props.name}. +25 points added as a return back gift.</p>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default StatusBar