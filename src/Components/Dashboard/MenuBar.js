import React, {Component} from 'react'

class MenuBar extends Component {
    constructor(props) {
        super(props);
        this.state = { 
           userStats : {followers:'10', following:'15', request:'12', classes: '0', events : '10'}
         };
    }
    render() {
        return (
            <div className="container menubar-wrapper">
                <div className="post card">
                    <div className="card-content">
                        <div className="row">
                            <div className="col m2 s12">
                                following<br />
                                {this.state.userStats.following}
                            </div>
                            <div className="col m2 s12">
                                followers <br />
                                {this.state.userStats.followers}
                            </div>
                            <div className="col m3 s12">
                                request<br />
                                {this.state.userStats.request}
                            </div>
                            <div className="col m2 s12">
                                classes<br />
                                {this.state.userStats.classes}
                            </div>
                            <div className="col m3 s12">
                                upcoming event<br />
                                {this.state.userStats.events}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MenuBar;